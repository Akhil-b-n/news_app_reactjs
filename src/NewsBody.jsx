import React from "react"
import { useEffect, useState } from "react"
import MainNews from "./NewsBody/MainNews"
import SubNews from "./NewsBody/SubNews"
import axios from "axios"

function NewsBody(){
    
    const [news, setNews] = useState([])
    useEffect( () => {
        const key = import.meta.env.VITE_key
        axios.get(` https://gnews.io/api/v4/search?q=politics&apikey=${key}`)
        .then(data => setNews(data.data.articles))
        .catch(error => console.log(error))
    },[])
return(
    <>
        <main>
            <section className="container mx-auto my-5">
            <h2 className="text-xl font-bold"> Top News</h2>
            {
                news.length > 0 ? (<div className="flex flex-col  gap-4 md:grid md:grid-cols-2 mt-6"> <MainNews article={news[0]} /> 
               <SubNews article={news[1]} />
               <SubNews article={news[2]} />
               <SubNews article={news[3]} />
                </div> )
                :( <p>Loading...</p>)

            }  
            
            
            </section>
        </main>
    </>
    )
}
export default NewsBody