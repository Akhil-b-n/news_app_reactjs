import axios from 'axios'
import './App.css'
import NewsBody from './NewsBody'
import Header from './header'
import { useEffect } from 'react'

function App(event) {
  event.preventdefault
  return (
    <>
     <Header />
    <NewsBody />

    </>
  )
}

export default App
