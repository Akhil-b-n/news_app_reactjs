import React from "react";

function SubNews(props){
const article= props.article
    return (
        <>
        <article className=" col-start-2 col-end-2 row-span-1">
            <a href={article.url} className="flex flex-row items-center justify-center gap-3">
                <img className="w-1/4" src={article.image} alt="" />
                <div className="flex flex-col gap-3">
                    <h2>{article.title} </h2>
                    <p> {article.description}
                    </p>
                </div>
            </a>
        </article>
        </>
    )
}
export default SubNews
