import React from "react";

function MainNews(props){
    const article =props.article
   
    return(
        <>
        <article className="md:row-start-1 md:row-end-4 md:col-start-1 md:col-end-1  ">
            <a className=" gap-4 flex flex-col" href={article.url}>
                <img src={article.image} alt="" />
                <div className="flex flex-col gap-3 mt-2">
                    <h2>{article.title}</h2>
                    <p> {article.description}
                    </p>
                </div>
            </a>
        </article>
        </>
    )
}
export default MainNews