import CategoriesDropdown from "./Dropdown"

function Header(){
    return(
    <header className="bg-[#808080]">
        <div className="container mx-auto p-5 md:px-0 flex flex-row items-center justify-between ">
        <a href="#"><h1 className="text-xl font-bold">Media center</h1></a>
        
        <nav className=" flex flex-row items-center" >
            <div>
                <span className="material-symbols-outlined md:hidden">
                    menu
                </span>
            </div>
            <ul className="hidden md:flex md:flex-row md:items-center md:justify-between md:gap-5 "> 
                <li><a href="#">About</a></li>
                <li ><CategoriesDropdown /></li>                 
                <li><a href="#">Contact</a></li>
                <li><a href="#">Subscription</a></li>
            </ul>
        </nav>
        </div>
    </header>    
    )
}
export default Header